{{/* vim: set filetype=mustache: */}}

{{- define "ingress.name" -}}
{{- default "name-virtual-host-ingress" .Values.ingress.name -}}
{{- end -}}

{{- define "apiVersion" -}}
{{- default "networking.k8s.io/v1" .Values.apiVersion -}}
{{- end -}}

{{- define "namespace" -}}
{{- default "spring-lab" .Values.namespace -}}
{{- end -}}
